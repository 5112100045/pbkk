--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.5
-- Started on 2015-05-10 17:18:07

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 188 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2077 (class 0 OID 0)
-- Dependencies: 188
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 174 (class 1259 OID 24731)
-- Name: DataRujukan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "DataRujukan" (
    "IdRujukan" character varying(40) NOT NULL,
    "IdTenagaMedis" character varying(40),
    "IdSarana" character varying(40),
    "Inserted" timestamp without time zone,
    "Updated" timestamp without time zone,
    "Deleted" timestamp without time zone,
    "IsiRujukan" character varying(500),
    "TanggalRujukan" date,
    "KeadaanPasien" character varying(100)
);


ALTER TABLE public."DataRujukan" OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 24706)
-- Name: PesertaJaminanKesehatan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "PesertaJaminanKesehatan" (
    "IdPeserta" character varying(40) NOT NULL,
    "Inserted" timestamp without time zone,
    "Updated" timestamp without time zone,
    "Deleted" timestamp without time zone,
    "NamaSarana" character varying(30),
    "JenisKepesertaan" character varying(25),
    "JenisKelamin" character varying(15)
);


ALTER TABLE public."PesertaJaminanKesehatan" OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 24754)
-- Name: RekamMedis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "RekamMedis" (
    "IdRekamMedis" character varying(40) NOT NULL,
    "IdTanggungan" character varying(40),
    "IdSarana" character varying(40),
    "Inserted" timestamp without time zone,
    "Updated" timestamp without time zone,
    "Deleted" timestamp without time zone,
    "TglTransaksi" timestamp without time zone,
    "Tempat" character varying(50),
    "NamaPasien" character varying(30),
    "Diagnosa" character varying(50),
    "Pemeriksa" character varying(30),
    "Keluhan" character varying(100),
    "Resep" character varying(300),
    "KeadaanPasienMasuk" character varying(50),
    "KeadaanPasienKeluar" character varying(50),
    "Tindakan" character varying(50),
    "IdPeserta" character varying(40)
);


ALTER TABLE public."RekamMedis" OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 24711)
-- Name: SaranaKesehatan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "SaranaKesehatan" (
    "IdSarana" character varying(40) NOT NULL,
    "IdJenisSarana" character varying(40),
    "Inserted" timestamp without time zone,
    "Updated" timestamp without time zone,
    "Deleted" timestamp without time zone,
    "NamaSarana" character varying(30),
    "AlamatSarana" character varying(100),
    "KotaSarana" character varying(50),
    "ProvinsiSarana" character varying(50)
);


ALTER TABLE public."SaranaKesehatan" OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 24721)
-- Name: Tanggungan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "Tanggungan" (
    "IdTanggungan" character varying(40) NOT NULL,
    "IdPeserta" character varying(40),
    "Inserted" timestamp without time zone,
    "Updated" timestamp without time zone,
    "Deleted" timestamp without time zone,
    "KepalaKeluarga" smallint,
    "HubunganKeluarga" character varying(25),
    "JenisKelamin" character varying(15)
);


ALTER TABLE public."Tanggungan" OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 24744)
-- Name: TenagaMedis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "TenagaMedis" (
    "IdTenagaMedis" character varying(40) NOT NULL,
    "IdSarana" character varying(40),
    "Inserted" timestamp without time zone,
    "Updated" timestamp without time zone,
    "Deleted" timestamp without time zone,
    "NamaTenagaMedis" character varying(30),
    "AlamatTenagaMedis" character varying(30),
    "Profesi" character varying(25),
    "Golongan" integer,
    "AlamatInstansi" character varying(50),
    "KotaInstansi" character varying(50),
    "ProvinsiInstansi" character varying(50)
);


ALTER TABLE public."TenagaMedis" OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 25964)
-- Name: datarrujukan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE datarrujukan (
    idrujukan character varying(255) NOT NULL,
    deleted timestamp without time zone,
    inserted timestamp without time zone,
    isirujukan character varying(255),
    keadaanpasien character varying(255),
    tanggalrujukan bytea,
    updated timestamp without time zone
);


ALTER TABLE public.datarrujukan OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 24701)
-- Name: jenisSarana; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "jenisSarana" (
    "IdJenisSarana" character varying(40) NOT NULL,
    "NamaJenisSarana" character varying(30)
);


ALTER TABLE public."jenisSarana" OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 25972)
-- Name: jenissarana; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE jenissarana (
    idjenissarana bytea NOT NULL,
    namajenissarana character varying(30) NOT NULL
);


ALTER TABLE public.jenissarana OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 25324)
-- Name: peserta_jaminan_kesehatan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE peserta_jaminan_kesehatan (
    id_peserta bytea NOT NULL,
    jenis_kepesertaan character varying(255),
    nama character varying(255)
);


ALTER TABLE public.peserta_jaminan_kesehatan OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 25980)
-- Name: pesertajaminankesehatan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pesertajaminankesehatan (
    idpeserta character varying(255) NOT NULL,
    deleted timestamp without time zone,
    inserted timestamp without time zone,
    jeniskelamin character varying(255),
    jeniskepesertaan character varying(255),
    namasarana character varying(255),
    updated timestamp without time zone
);


ALTER TABLE public.pesertajaminankesehatan OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 25340)
-- Name: rekam_medis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rekam_medis (
    id_rekam_medis bytea NOT NULL,
    diagnosa character varying(255),
    id_peserta bytea,
    id_sarana bytea,
    nama_pasien character varying(255),
    pemeriksa character varying(255),
    tempat character varying(255),
    tgl_transaksi timestamp without time zone
);


ALTER TABLE public.rekam_medis OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 25988)
-- Name: rekammedis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rekammedis (
    idrekammedis character varying(255) NOT NULL,
    deleted timestamp without time zone,
    diagnosa character varying(255),
    idpeserta bytea,
    idsarana bytea,
    idtanggungan bytea,
    inserted timestamp without time zone,
    keadaanpasienmasuk character varying(255),
    keadaanpasienkeluar character varying(255),
    keluhan character varying(255),
    namapasien character varying(255),
    pemeriksa character varying(255),
    resep character varying(255),
    tempat character varying(255),
    tindakan character varying(255),
    updated timestamp without time zone,
    tgltransaksi timestamp without time zone
);


ALTER TABLE public.rekammedis OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 25348)
-- Name: sarana_kesehatan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sarana_kesehatan (
    id_sarana bytea NOT NULL,
    alamat_sarana character varying(100),
    kota_sarana character varying(50),
    nama_sarana character varying(30) NOT NULL,
    provinsi_sarana character varying(50)
);


ALTER TABLE public.sarana_kesehatan OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 25996)
-- Name: saranakesehatan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE saranakesehatan (
    idsarana character varying(255) NOT NULL,
    alamatsarana character varying(100),
    deleted timestamp without time zone,
    idjenissarana character varying(255),
    inserted timestamp without time zone,
    kotasarana character varying(50),
    namasarana character varying(30) NOT NULL,
    provinsisarana character varying(50),
    updated timestamp without time zone
);


ALTER TABLE public.saranakesehatan OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 26004)
-- Name: tanggungan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tanggungan (
    idtanggungan bytea NOT NULL,
    deleted timestamp without time zone,
    hubungankeluarga character varying(255),
    idpeserta bytea,
    inserted timestamp without time zone,
    jeniskelamin character varying(255),
    kepalakeluarga boolean,
    updated timestamp without time zone
);


ALTER TABLE public.tanggungan OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 25332)
-- Name: tenaga_medis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tenaga_medis (
    id_tenaga bytea NOT NULL,
    alamat_instansi character varying(255),
    alamat_tenaga character varying(255),
    golongan character varying(255),
    id_sarana bytea,
    kota_instansi character varying(255),
    nama_tenaga character varying(255),
    profesi character varying(255),
    provinsi_instansi character varying(255)
);


ALTER TABLE public.tenaga_medis OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 26012)
-- Name: tenagamedis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tenagamedis (
    idtenagamedis character varying(255) NOT NULL,
    alamatinstansi character varying(255),
    alamattenagamedis character varying(255),
    deleted timestamp without time zone,
    golongan character varying(255),
    inserted timestamp without time zone,
    kotainstansi character varying(255),
    namatenagamedis character varying(255),
    profesi character varying(255),
    provinsiinstansi character varying(255),
    updated timestamp without time zone,
    idsarana character varying(255)
);


ALTER TABLE public.tenagamedis OWNER TO postgres;

--
-- TOC entry 2056 (class 0 OID 24731)
-- Dependencies: 174
-- Data for Name: DataRujukan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "DataRujukan" ("IdRujukan", "IdTenagaMedis", "IdSarana", "Inserted", "Updated", "Deleted", "IsiRujukan", "TanggalRujukan", "KeadaanPasien") FROM stdin;
\.


--
-- TOC entry 2053 (class 0 OID 24706)
-- Dependencies: 171
-- Data for Name: PesertaJaminanKesehatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "PesertaJaminanKesehatan" ("IdPeserta", "Inserted", "Updated", "Deleted", "NamaSarana", "JenisKepesertaan", "JenisKelamin") FROM stdin;
\.


--
-- TOC entry 2058 (class 0 OID 24754)
-- Dependencies: 176
-- Data for Name: RekamMedis; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "RekamMedis" ("IdRekamMedis", "IdTanggungan", "IdSarana", "Inserted", "Updated", "Deleted", "TglTransaksi", "Tempat", "NamaPasien", "Diagnosa", "Pemeriksa", "Keluhan", "Resep", "KeadaanPasienMasuk", "KeadaanPasienKeluar", "Tindakan", "IdPeserta") FROM stdin;
\.


--
-- TOC entry 2054 (class 0 OID 24711)
-- Dependencies: 172
-- Data for Name: SaranaKesehatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "SaranaKesehatan" ("IdSarana", "IdJenisSarana", "Inserted", "Updated", "Deleted", "NamaSarana", "AlamatSarana", "KotaSarana", "ProvinsiSarana") FROM stdin;
\.


--
-- TOC entry 2055 (class 0 OID 24721)
-- Dependencies: 173
-- Data for Name: Tanggungan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Tanggungan" ("IdTanggungan", "IdPeserta", "Inserted", "Updated", "Deleted", "KepalaKeluarga", "HubunganKeluarga", "JenisKelamin") FROM stdin;
\.


--
-- TOC entry 2057 (class 0 OID 24744)
-- Dependencies: 175
-- Data for Name: TenagaMedis; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "TenagaMedis" ("IdTenagaMedis", "IdSarana", "Inserted", "Updated", "Deleted", "NamaTenagaMedis", "AlamatTenagaMedis", "Profesi", "Golongan", "AlamatInstansi", "KotaInstansi", "ProvinsiInstansi") FROM stdin;
\.


--
-- TOC entry 2063 (class 0 OID 25964)
-- Dependencies: 181
-- Data for Name: datarrujukan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY datarrujukan (idrujukan, deleted, inserted, isirujukan, keadaanpasien, tanggalrujukan, updated) FROM stdin;
\.


--
-- TOC entry 2052 (class 0 OID 24701)
-- Dependencies: 170
-- Data for Name: jenisSarana; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "jenisSarana" ("IdJenisSarana", "NamaJenisSarana") FROM stdin;
\.


--
-- TOC entry 2064 (class 0 OID 25972)
-- Dependencies: 182
-- Data for Name: jenissarana; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY jenissarana (idjenissarana, namajenissarana) FROM stdin;
\\xc194890c48874402af46fe52a5b6e516	haha
\.


--
-- TOC entry 2059 (class 0 OID 25324)
-- Dependencies: 177
-- Data for Name: peserta_jaminan_kesehatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY peserta_jaminan_kesehatan (id_peserta, jenis_kepesertaan, nama) FROM stdin;
\.


--
-- TOC entry 2065 (class 0 OID 25980)
-- Dependencies: 183
-- Data for Name: pesertajaminankesehatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pesertajaminankesehatan (idpeserta, deleted, inserted, jeniskelamin, jeniskepesertaan, namasarana, updated) FROM stdin;
\.


--
-- TOC entry 2061 (class 0 OID 25340)
-- Dependencies: 179
-- Data for Name: rekam_medis; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY rekam_medis (id_rekam_medis, diagnosa, id_peserta, id_sarana, nama_pasien, pemeriksa, tempat, tgl_transaksi) FROM stdin;
\.


--
-- TOC entry 2066 (class 0 OID 25988)
-- Dependencies: 184
-- Data for Name: rekammedis; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY rekammedis (idrekammedis, deleted, diagnosa, idpeserta, idsarana, idtanggungan, inserted, keadaanpasienmasuk, keadaanpasienkeluar, keluhan, namapasien, pemeriksa, resep, tempat, tindakan, updated, tgltransaksi) FROM stdin;
\.


--
-- TOC entry 2062 (class 0 OID 25348)
-- Dependencies: 180
-- Data for Name: sarana_kesehatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY sarana_kesehatan (id_sarana, alamat_sarana, kota_sarana, nama_sarana, provinsi_sarana) FROM stdin;
\.


--
-- TOC entry 2067 (class 0 OID 25996)
-- Dependencies: 185
-- Data for Name: saranakesehatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY saranakesehatan (idsarana, alamatsarana, deleted, idjenissarana, inserted, kotasarana, namasarana, provinsisarana, updated) FROM stdin;
\.


--
-- TOC entry 2068 (class 0 OID 26004)
-- Dependencies: 186
-- Data for Name: tanggungan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tanggungan (idtanggungan, deleted, hubungankeluarga, idpeserta, inserted, jeniskelamin, kepalakeluarga, updated) FROM stdin;
\.


--
-- TOC entry 2060 (class 0 OID 25332)
-- Dependencies: 178
-- Data for Name: tenaga_medis; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tenaga_medis (id_tenaga, alamat_instansi, alamat_tenaga, golongan, id_sarana, kota_instansi, nama_tenaga, profesi, provinsi_instansi) FROM stdin;
\.


--
-- TOC entry 2069 (class 0 OID 26012)
-- Dependencies: 187
-- Data for Name: tenagamedis; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tenagamedis (idtenagamedis, alamatinstansi, alamattenagamedis, deleted, golongan, inserted, kotainstansi, namatenagamedis, profesi, provinsiinstansi, updated, idsarana) FROM stdin;
\.


--
-- TOC entry 1903 (class 2606 OID 24705)
-- Name: IDJenisSarana; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "jenisSarana"
    ADD CONSTRAINT "IDJenisSarana" PRIMARY KEY ("IdJenisSarana");


--
-- TOC entry 1905 (class 2606 OID 24710)
-- Name: IdPeserta; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "PesertaJaminanKesehatan"
    ADD CONSTRAINT "IdPeserta" PRIMARY KEY ("IdPeserta");


--
-- TOC entry 1915 (class 2606 OID 24761)
-- Name: IdRekamMedis; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "RekamMedis"
    ADD CONSTRAINT "IdRekamMedis" PRIMARY KEY ("IdRekamMedis");


--
-- TOC entry 1911 (class 2606 OID 24738)
-- Name: IdRujukan; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "DataRujukan"
    ADD CONSTRAINT "IdRujukan" PRIMARY KEY ("IdRujukan");


--
-- TOC entry 1907 (class 2606 OID 24715)
-- Name: IdSarana; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "SaranaKesehatan"
    ADD CONSTRAINT "IdSarana" PRIMARY KEY ("IdSarana");


--
-- TOC entry 1909 (class 2606 OID 24725)
-- Name: IdTanggungan; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "Tanggungan"
    ADD CONSTRAINT "IdTanggungan" PRIMARY KEY ("IdTanggungan");


--
-- TOC entry 1913 (class 2606 OID 24748)
-- Name: IdTenagaMedis; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "TenagaMedis"
    ADD CONSTRAINT "IdTenagaMedis" PRIMARY KEY ("IdTenagaMedis");


--
-- TOC entry 1925 (class 2606 OID 25971)
-- Name: datarrujukan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY datarrujukan
    ADD CONSTRAINT datarrujukan_pkey PRIMARY KEY (idrujukan);


--
-- TOC entry 1927 (class 2606 OID 25979)
-- Name: jenissarana_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY jenissarana
    ADD CONSTRAINT jenissarana_pkey PRIMARY KEY (idjenissarana);


--
-- TOC entry 1917 (class 2606 OID 25331)
-- Name: peserta_jaminan_kesehatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY peserta_jaminan_kesehatan
    ADD CONSTRAINT peserta_jaminan_kesehatan_pkey PRIMARY KEY (id_peserta);


--
-- TOC entry 1929 (class 2606 OID 25987)
-- Name: pesertajaminankesehatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pesertajaminankesehatan
    ADD CONSTRAINT pesertajaminankesehatan_pkey PRIMARY KEY (idpeserta);


--
-- TOC entry 1921 (class 2606 OID 25347)
-- Name: rekam_medis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rekam_medis
    ADD CONSTRAINT rekam_medis_pkey PRIMARY KEY (id_rekam_medis);


--
-- TOC entry 1931 (class 2606 OID 25995)
-- Name: rekammedis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rekammedis
    ADD CONSTRAINT rekammedis_pkey PRIMARY KEY (idrekammedis);


--
-- TOC entry 1923 (class 2606 OID 25355)
-- Name: sarana_kesehatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sarana_kesehatan
    ADD CONSTRAINT sarana_kesehatan_pkey PRIMARY KEY (id_sarana);


--
-- TOC entry 1933 (class 2606 OID 26003)
-- Name: saranakesehatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY saranakesehatan
    ADD CONSTRAINT saranakesehatan_pkey PRIMARY KEY (idsarana);


--
-- TOC entry 1935 (class 2606 OID 26011)
-- Name: tanggungan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tanggungan
    ADD CONSTRAINT tanggungan_pkey PRIMARY KEY (idtanggungan);


--
-- TOC entry 1919 (class 2606 OID 25339)
-- Name: tenaga_medis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tenaga_medis
    ADD CONSTRAINT tenaga_medis_pkey PRIMARY KEY (id_tenaga);


--
-- TOC entry 1937 (class 2606 OID 26019)
-- Name: tenagamedis_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tenagamedis
    ADD CONSTRAINT tenagamedis_pkey PRIMARY KEY (idtenagamedis);


--
-- TOC entry 1938 (class 2606 OID 24716)
-- Name: IdJenisSarana; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "jenisSarana"
    ADD CONSTRAINT "IdJenisSarana" FOREIGN KEY ("IdJenisSarana") REFERENCES "jenisSarana"("IdJenisSarana");


--
-- TOC entry 1939 (class 2606 OID 24726)
-- Name: IdPeserta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Tanggungan"
    ADD CONSTRAINT "IdPeserta" FOREIGN KEY ("IdPeserta") REFERENCES "PesertaJaminanKesehatan"("IdPeserta");


--
-- TOC entry 1944 (class 2606 OID 24772)
-- Name: IdPeserta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "RekamMedis"
    ADD CONSTRAINT "IdPeserta" FOREIGN KEY ("IdPeserta") REFERENCES "SaranaKesehatan"("IdSarana");


--
-- TOC entry 1940 (class 2606 OID 24739)
-- Name: IdSarana; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "DataRujukan"
    ADD CONSTRAINT "IdSarana" FOREIGN KEY ("IdSarana") REFERENCES "SaranaKesehatan"("IdSarana");


--
-- TOC entry 1941 (class 2606 OID 24749)
-- Name: IdSarana; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "TenagaMedis"
    ADD CONSTRAINT "IdSarana" FOREIGN KEY ("IdSarana") REFERENCES "SaranaKesehatan"("IdSarana");


--
-- TOC entry 1943 (class 2606 OID 24767)
-- Name: IdSarana; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "RekamMedis"
    ADD CONSTRAINT "IdSarana" FOREIGN KEY ("IdSarana") REFERENCES "SaranaKesehatan"("IdSarana");


--
-- TOC entry 1942 (class 2606 OID 24762)
-- Name: IdTanggungan; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "RekamMedis"
    ADD CONSTRAINT "IdTanggungan" FOREIGN KEY ("IdTanggungan") REFERENCES "Tanggungan"("IdTanggungan");


--
-- TOC entry 2076 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-05-10 17:18:08

--
-- PostgreSQL database dump complete
--

