package id.its.pbkk.FP.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.FP.dao.JenisSaranaDao;
import id.its.pbkk.FP.domain.JenisSarana;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("jenisSaranaDao")
public class HibJenisSaranaDao implements JenisSaranaDao {
	private SessionFactory sessionFactory;
	
	public HibJenisSaranaDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	@Transactional(readOnly=true)
	public List<JenisSarana> list() {
		List<JenisSarana> jenissarana = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from JenisSarana")
				.list();
		return jenissarana;
	}

	public JenisSarana findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<JenisSarana> findByJenisSarana(String nama) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(JenisSarana jenisSarana) {
		this.getSessionFactory().getCurrentSession().save(jenisSarana);
		
	}

	public void delete(JenisSarana jenisSarana) {
		this.getSessionFactory().getCurrentSession().delete(jenisSarana);
		
	}
}
