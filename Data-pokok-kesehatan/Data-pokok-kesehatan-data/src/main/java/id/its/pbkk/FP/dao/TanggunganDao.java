package id.its.pbkk.FP.dao;

import java.util.List;

import id.its.pbkk.FP.domain.Tanggungan;

public interface TanggunganDao {
	public List<Tanggungan> list();
	public List<Tanggungan> findByIdPeserta(String IdPeserta);
	public Tanggungan findByIdTanggungan(String IdTanggungan);
	public Tanggungan findByKepalaKeluarga(String KepalaKeluarga);
	public Tanggungan findByJenisKelamin(String JenisKelamin);
	public void save(Tanggungan tanggungan);
	public void delete(Tanggungan tanggungan);
}
