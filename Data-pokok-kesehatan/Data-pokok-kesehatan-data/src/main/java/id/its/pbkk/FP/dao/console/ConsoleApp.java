package id.its.pbkk.FP.dao.console;

import java.util.Date;
import java.util.List;

import id.its.pbkk.FP.dao.DataRujukanDao;
import id.its.pbkk.FP.dao.JenisSaranaDao;
import id.its.pbkk.FP.domain.DataRujukan;
import id.its.pbkk.FP.domain.JenisSarana;
import id.its.pbkk.FP.dao.PesertaJaminanKesehatanDao;
import id.its.pbkk.FP.domain.PesertaJaminanKesehatan;

import org.joda.time.DateTime;
import org.springframework.context.support.GenericXmlApplicationContext;

public class ConsoleApp {
	public static void main(String[] args) 
	{
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
		ctx.load("classpath:datasource.xml");
		ctx.refresh();

		JenisSaranaDao jenisSaranaDao = ctx.getBean("jenisSaranaDao", JenisSaranaDao.class);
		PesertaJaminanKesehatanDao pesertaJaminanKesehatanDao = ctx.getBean("pesertaJaminanKesehatanDao", PesertaJaminanKesehatanDao.class);
		DataRujukanDao dataRujukanDao = ctx.getBean("dataRujukanDao", DataRujukanDao.class);

		//Peserta aminan kesehatan
		PesertaJaminanKesehatan p1 = new PesertaJaminanKesehatan("zola","Premiun","laki-laki");
		PesertaJaminanKesehatan p2 = new PesertaJaminanKesehatan("alif","pertamax","laki-laki");
		PesertaJaminanKesehatan p3 = new PesertaJaminanKesehatan("hapsari","solar","perempuan");

		//Peserta aminan kesehatan save
		pesertaJaminanKesehatanDao.save(p1);
		pesertaJaminanKesehatanDao.save(p2);
		pesertaJaminanKesehatanDao.save(p3);

		List<PesertaJaminanKesehatan> peserta = pesertaJaminanKesehatanDao.list();
		PesertaJaminanKesehatan peserta2;
		//Find by name
		peserta = pesertaJaminanKesehatanDao.findByName("hapsari");
		for (PesertaJaminanKesehatan p : peserta) {
			System.out.println(p);
		}
//
//		//Find by jeniskelamin
//		peserta = pesertaJaminanKesehatanDao.findByJenisKelamin("laki-laki");
//		for (PesertaJaminanKesehatan p : peserta) {
//			System.out.println(p);
//		}

		
		//Data Rujukan 
//		DateTime date = new DateTime();
//		DataRujukan d1 = new DataRujukan("zola",date,"laki-laki");
//		DataRujukan d2 = new DataRujukan("sd",date,"laki-laki");
//		
//		//Data Rujukan save
//		dataRujukanDao.save(d1);
//		dataRujukanDao.save(d2);
		/*
		 
		List<DataRujukan> dataRujukan = dataRujukanDao.list();
		dataRujukan = DataRujukanDao.findByName("zola");
		for (DataRujukan p : dataRujukan) 
		{
			System.out.println(p);
		}
		*/
		
		
		



	}
}