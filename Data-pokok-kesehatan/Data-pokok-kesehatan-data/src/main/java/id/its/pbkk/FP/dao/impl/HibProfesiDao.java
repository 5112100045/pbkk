package id.its.pbkk.FP.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.FP.dao.ProfesiDao;
import id.its.pbkk.FP.domain.Profesi;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("profesiDao")
public class HibProfesiDao implements ProfesiDao {

private SessionFactory sessionFactory;
	
	public HibProfesiDao() {
		// TODO Auto-generated constructor stub
	}

	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Transactional(readOnly=true)
	public List<Profesi> list() {
		List<Profesi> profesi = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from Profesi")
				.list();
		return profesi;
	}

	public Profesi findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Profesi> findByName(String Nama) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(Profesi profesi) {
		this.getSessionFactory().getCurrentSession().save(profesi);
		
	}

	public void delete(Profesi profesi) {
		this.getSessionFactory().getCurrentSession().delete(profesi);
		
	}
}
