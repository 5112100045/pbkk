package id.its.pbkk.FP.dao;
import id.its.pbkk.FP.domain.Golongan;

import java.util.List;


public interface GolonganDao {

	public List<Golongan> list();
	public Golongan findById(String id);
	public List<Golongan> findByName(String Nama);
	
}
