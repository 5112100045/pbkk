package id.its.pbkk.FP.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.FP.dao.DataRujukanDao;
import id.its.pbkk.FP.domain.DataRujukan;

import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("dataRujukanDao")
public class HibDataRujukanDao implements DataRujukanDao {

	private SessionFactory sessionFactory;
	
	public HibDataRujukanDao()
	{
		
	}
	

	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Transactional(readOnly=true)
	public List<DataRujukan> list() {
		List<DataRujukan> datarujukan = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from DataRujukan")
				.list();
		return datarujukan;
	}
	
	@Transactional(readOnly=true)
	public DataRujukan findById(String id) {
		DataRujukan datarujukan = (DataRujukan) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from DataRujukan c where c.id like :id")
				.setParameter("id", "%" + id + "%");
		
		return datarujukan;
	}

	public List<DataRujukan> findByName(String nama) {
		List<DataRujukan> datarujukan = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from DataRujukan c where lower(c.nama) like lower(:nama)")
				.setParameter("nama", "%" + nama + "%")
				.list();
		
		return datarujukan;
	}

	@Transactional(readOnly=true)
	public List<DataRujukan> findByTanggal(DateTime tanggal) {
		List<DataRujukan> datarujukan = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from DataRujukan c where c.tanggal like :tanggal")
				.setParameter("tanggal", "%" + tanggal + "%")
				.list();
		
		return datarujukan;
	}

	@Transactional(readOnly=true)
	public List<DataRujukan> findByKepesertaan(String kepesertaan) {
		List<DataRujukan> datarujukan = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from DataRujukan c where lower(c.kepesertaan) like lower(:kepesertaan)")
				.setParameter("kepesertaan", "%" + kepesertaan + "%")
				.list();
		
		return datarujukan;
	}

	public void save(DataRujukan dataRujukan) {
		this.getSessionFactory().getCurrentSession().save(dataRujukan);
		
	}

	public void delete(DataRujukan dataRujukan) {
		this.getSessionFactory().getCurrentSession().delete(dataRujukan);
		
	}

}
