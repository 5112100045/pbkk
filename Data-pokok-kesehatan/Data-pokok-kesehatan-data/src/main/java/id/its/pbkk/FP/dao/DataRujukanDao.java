package id.its.pbkk.FP.dao;
import id.its.pbkk.FP.domain.DataRujukan;
import org.joda.time.DateTime;
import java.util.List;

public interface DataRujukanDao {
	
	public List<DataRujukan> list();
	public DataRujukan findById(String id);
	public List<DataRujukan> findByName(String nama);
	public List<DataRujukan> findByTanggal(DateTime Tanggal);
	public List<DataRujukan> findByKepesertaan(String Kepesertaan);
	public void save(DataRujukan dataRujukan);
	public void delete(DataRujukan dataRujukan);
}
