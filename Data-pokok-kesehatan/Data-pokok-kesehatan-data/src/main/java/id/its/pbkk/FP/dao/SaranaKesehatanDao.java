package id.its.pbkk.FP.dao;

import java.util.List;

import id.its.pbkk.FP.domain.SaranaKesehatan;

public interface SaranaKesehatanDao {
	public List<SaranaKesehatan> list();
	public SaranaKesehatan findById(String id);
	public List<SaranaKesehatan> findByKota(String KotaSarana);
	public List<SaranaKesehatan> findByProvinsi(String ProvinsiSarana);
	public List<SaranaKesehatan> findByJenis(String JenisSarana);
	public void save(SaranaKesehatan SaranaKesehatan);
	public void delete(SaranaKesehatan SaranaKesehatan);
	
}
