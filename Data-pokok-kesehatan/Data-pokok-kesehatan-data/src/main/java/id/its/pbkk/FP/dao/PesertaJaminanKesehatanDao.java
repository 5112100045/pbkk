package id.its.pbkk.FP.dao;
import id.its.pbkk.FP.domain.PesertaJaminanKesehatan;

import java.util.List;

public interface PesertaJaminanKesehatanDao {
	public List<PesertaJaminanKesehatan> list();
	public PesertaJaminanKesehatan findById(String id);
	public List<PesertaJaminanKesehatan> findByName(String Nama);
	public List<PesertaJaminanKesehatan> findByJenisKelamin(String JenisKelamin);
	public List<PesertaJaminanKesehatan> findByKepesertaan(String Kepesertaan);
	public void save(PesertaJaminanKesehatan pesertaJaskes);
	public void delete(PesertaJaminanKesehatan pesertaJaskes);
}
