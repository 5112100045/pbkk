package id.its.pbkk.FP.dao;

import id.its.pbkk.FP.domain.TenagaMedis;

import java.util.List;

public interface TenagaMedisDao {
	public List<TenagaMedis> list();
	public TenagaMedis findById(String id);
	public List<TenagaMedis> findByName(String Nama);
	public List<TenagaMedis> findByProfesi(String Profesi);
	public List<TenagaMedis> findByKota(String KotaInstansi);
	public List<TenagaMedis> findByProvinsi(String ProvinsiInstansi);
	public List<TenagaMedis> findByGolongan(String Golongan);
	public void save(TenagaMedis tenagaMedis);
	public void delete(TenagaMedis tenagaMedis);
}
