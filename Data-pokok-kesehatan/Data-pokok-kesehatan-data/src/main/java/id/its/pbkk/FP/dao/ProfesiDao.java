package id.its.pbkk.FP.dao;
import id.its.pbkk.FP.domain.Profesi;

import java.util.List;


public interface ProfesiDao {
	
	public List<Profesi> list();
	public Profesi findById(String id);
	public List<Profesi> findByName(String Nama);
}
