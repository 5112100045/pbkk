package id.its.pbkk.FP.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.FP.dao.SaranaKesehatanDao;
import id.its.pbkk.FP.domain.SaranaKesehatan;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("saranaKesehatan")
public class HibSaranaKesehatanDao implements SaranaKesehatanDao{

	private SessionFactory sessionFactory;

	public HibSaranaKesehatanDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Transactional(readOnly=true)
	public List<SaranaKesehatan> list() {
		List<SaranaKesehatan> sk = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from SaranaKesehatan")
				.list();
		return sk;
	}

	@Transactional(readOnly=true)
	public SaranaKesehatan findById(String id) {
		SaranaKesehatan sk = (SaranaKesehatan) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from SaranaKesehatan c where c.id like :id")
				.setParameter("id", "%" + id + "%");
		
		return sk;
	}

	@Transactional(readOnly=true)
	public List<SaranaKesehatan> findByKota(String kotasarana) {
		List<SaranaKesehatan> sk = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from SaranaKesehatan c where lower(c.kotasarana) like lower(:kotasarana)")
				.setParameter("kotasarana", "%" + kotasarana + "%")
				.list();
		return sk;
	}

	@Transactional(readOnly=true)
	public List<SaranaKesehatan> findByProvinsi(String provinsisarana) {
		List<SaranaKesehatan> sk = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from ProvinsiSarana c where lower(c.provinsisarana) like lower(:provinsisarana)")
				.setParameter("provinsisarana", "%" + provinsisarana + "%")
				.list();
		return sk;
	}

	@Transactional(readOnly=true)
	public List<SaranaKesehatan> findByJenis(String JenisSarana) {
		List<SaranaKesehatan> sk = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from JenisSarana c where lower(c.JenisSarana) like lower(:JenisSarana)")
				.setParameter("JenisSarana", "%" + JenisSarana + "%")
				.list();
		return sk;
	}

	public void save(SaranaKesehatan SaranaKesehatan) {
		this.getSessionFactory().getCurrentSession().save(SaranaKesehatan);
		
	}

	public void delete(SaranaKesehatan SaranaKesehatan) {
		this.getSessionFactory().getCurrentSession().delete(SaranaKesehatan);
		
	}

}
