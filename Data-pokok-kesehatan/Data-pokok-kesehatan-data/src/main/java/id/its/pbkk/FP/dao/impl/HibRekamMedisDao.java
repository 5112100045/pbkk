package id.its.pbkk.FP.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.FP.dao.RekamMedisDao;
import id.its.pbkk.FP.domain.RekamMedis;

import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("rekamMedisDao")
public class HibRekamMedisDao implements RekamMedisDao {

	private SessionFactory sessionFactory;

	public HibRekamMedisDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Transactional(readOnly=true)
	public List<RekamMedis> list() {
		List<RekamMedis> rekammedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from RekamMedis")
				.list();
		return rekammedis;
	}

	@Transactional(readOnly=true)
	public List<RekamMedis> findByDate(DateTime tgltransaksi) {
		List<RekamMedis> rekammedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from RekamMedis c where c.tgltransaksi like :tgltransaksi")
				.setParameter("tgltransaksi", "%" + tgltransaksi + "%")
				.list();
		
		return rekammedis;
	}

	@Transactional(readOnly=true)
	public List<RekamMedis> findByName(String namaPasien) {
		List<RekamMedis> rekammedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from RekamMedis c where lower(c.namaPasien) like lower(:namaPasien)")
				.setParameter("namaPasien", "%" + namaPasien + "%")
				.list();
		
		return rekammedis;
	}

	@Transactional(readOnly=true)
	public List<RekamMedis> findByDiagnosa(String diagnosa) {
		List<RekamMedis> rekammedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from RekamMedis c where lower(c.diagnosa) like lower(:diagnosa)")
				.setParameter("diagnosa", "%" + diagnosa + "%")
				.list();
		
		return rekammedis;
	}

	@Transactional(readOnly=true)
	public List<RekamMedis> findByPemeriksa(String pemeriksa) {
		List<RekamMedis> rekammedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from RekamMedis c where lower(c.pemeriksa) like lower(:pemeriksa)")
				.setParameter("pemeriksa", "%" + pemeriksa + "%")
				.list();
		
		return rekammedis;
	}

	public void save(RekamMedis rekamMedis) {
		this.getSessionFactory().getCurrentSession().save(rekamMedis);
		
	}

	public void delete(RekamMedis rekamMedis) {
		this.getSessionFactory().getCurrentSession().delete(rekamMedis);
		
	}

}
