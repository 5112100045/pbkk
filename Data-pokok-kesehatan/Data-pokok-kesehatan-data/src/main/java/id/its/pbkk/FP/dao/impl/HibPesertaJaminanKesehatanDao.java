package id.its.pbkk.FP.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.FP.dao.PesertaJaminanKesehatanDao;
import id.its.pbkk.FP.domain.PesertaJaminanKesehatan;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("pesertaJaminanKesehatanDao")
public class HibPesertaJaminanKesehatanDao implements PesertaJaminanKesehatanDao{

	private SessionFactory sessionFactory;
	
	public HibPesertaJaminanKesehatanDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Transactional(readOnly=true)
	public List<PesertaJaminanKesehatan> list() {
		List<PesertaJaminanKesehatan> pjk = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from PesertaJaminanKesehatan")
				.list();
		return pjk;
	}

	@Transactional(readOnly=true)
	public PesertaJaminanKesehatan findById(String id) {
		PesertaJaminanKesehatan pjk = (PesertaJaminanKesehatan) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from PesertaJaminanKesehatan c where c.id like :id")
				.setParameter("id", "%" + id + "%");
		
		return pjk;
	}

	@Transactional(readOnly=true)
	public List<PesertaJaminanKesehatan> findByName(String namapeserta) {
		List<PesertaJaminanKesehatan> pjk = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from PesertaJaminanKesehatan c where lower(c.namapeserta) like lower(:namapeserta)")
				.setParameter("namapeserta", "%" + namapeserta + "%")
				.list();
		
		return pjk;
	}

	@Transactional(readOnly=true)
	public List<PesertaJaminanKesehatan> findByJenisKelamin(String jeniskelamin) {
		List<PesertaJaminanKesehatan> pjk = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from PesertaJaminanKesehatan c where lower(c.jeniskelamin) like lower(:jeniskelamin)")
				.setParameter("jeniskelamin", "%" + jeniskelamin + "%")
				.list();
		
		return pjk;
	}

	@Transactional(readOnly=true)
	public List<PesertaJaminanKesehatan> findByKepesertaan(String Kepesertaan) {
		List<PesertaJaminanKesehatan> pjk = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from PesertaJaminanKesehatan c where lower(c.Kepesertaan) like lower(:Kepesertaan)")
				.setParameter("Kepesertaan", "%" + Kepesertaan + "%")
				.list();
		
		return pjk;
	}

	public void save(PesertaJaminanKesehatan pesertaJaskes) {
		this.getSessionFactory().getCurrentSession().save(pesertaJaskes);
		
	}

	public void delete(PesertaJaminanKesehatan pesertaJaskes) {
		this.getSessionFactory().getCurrentSession().delete(pesertaJaskes);
		
	}

}