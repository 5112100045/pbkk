package id.its.pbkk.FP.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.FP.dao.TanggunganDao;
import id.its.pbkk.FP.domain.Tanggungan;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("tanggungan")
public class HibTanggunganDao implements TanggunganDao{

	private SessionFactory sessionFactory;

	public HibTanggunganDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Transactional(readOnly=true)
	public List<Tanggungan> list() {
		List<Tanggungan> tanggungan = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from Tanggungan")
				.list();
		return tanggungan;
	}

	@Transactional(readOnly=true)
	public List<Tanggungan> findByIdPeserta(String idpeserta) {
		Tanggungan tanggungan = (Tanggungan) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from Tanggungan c where c.idpeserta like :idpeserta")
				.setParameter("idpeserta", "%" + idpeserta + "%");
		
		return (List<Tanggungan>) tanggungan;
	}

	@Transactional(readOnly=true)
	public Tanggungan findByIdTanggungan(String idtanggungan) {
		Tanggungan tanggungan = (Tanggungan) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from Tanggungan c where c.idtanggungan like :idtanggungan")
				.setParameter("idtanggungan", "%" + idtanggungan + "%");
		
		return tanggungan;
	}

	@Transactional(readOnly=true)
	public Tanggungan findByKepalaKeluarga(String kepalakeluarga) {
		List<Tanggungan> tanggungan = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from Tanggungan c where lower(c.kepalakeluarga) like lower(:kepalakeluarga)")
				.setParameter("kepalakeluarga", "%" + kepalakeluarga + "%")
				.list();
		return (Tanggungan) tanggungan;
	}

	@Transactional(readOnly=true)
	public Tanggungan findByJenisKelamin(String jeniskelamin) {
		List<Tanggungan> tanggungan = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from Tanggungan c where lower(c.jeniskelamin) like lower(:jeniskelamin)")
				.setParameter("jeniskelamin", "%" + jeniskelamin + "%")
				.list();
		return (Tanggungan) tanggungan;
	}

	public void save(Tanggungan tanggungan) {
		this.getSessionFactory().getCurrentSession().save(tanggungan);
		
	}

	public void delete(Tanggungan tanggungan) {
		this.getSessionFactory().getCurrentSession().delete(tanggungan);
		
	}

}
