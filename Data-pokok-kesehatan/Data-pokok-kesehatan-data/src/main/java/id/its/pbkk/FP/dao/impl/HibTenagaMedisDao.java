package id.its.pbkk.FP.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.FP.dao.TenagaMedisDao;
import id.its.pbkk.FP.domain.TenagaMedis;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("tenagaMedis")
public class HibTenagaMedisDao implements TenagaMedisDao{

	private SessionFactory sessionFactory;

	public HibTenagaMedisDao()
	{
		
	}
	
	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Transactional(readOnly=true)
	public List<TenagaMedis> list() {
		List<TenagaMedis> tenagamedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from TenagaMedis")
				.list();
		return tenagamedis;
	}

	@Transactional(readOnly=true)
	public TenagaMedis findById(String id) {
		TenagaMedis tenagamedis = (TenagaMedis) this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from TenagaMedis c where c.id like :id")
				.setParameter("id", "%" + id + "%");
		
		return tenagamedis;
	}

	@Transactional(readOnly=true)
	public List<TenagaMedis> findByName(String nama) {
		List<TenagaMedis> tenagamedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from TenagaMedis c where lower(c.nama) like lower(:nama)")
				.setParameter("nama", "%" + nama + "%")
				.list();
		return tenagamedis;
	}

	@Transactional(readOnly=true)
	public List<TenagaMedis> findByProfesi(String profesi) {
		List<TenagaMedis> tenagamedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from TenagaMedis c where lower(c.profesi) like lower(:profesi)")
				.setParameter("profesi", "%" + profesi + "%")
				.list();
		return tenagamedis;
	}

	@Transactional(readOnly=true)
	public List<TenagaMedis> findByKota(String kotainstansi) {
		List<TenagaMedis> tenagamedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from TenagaMedis c where lower(c.kotainstansi) like lower(:kotainstansi)")
				.setParameter("kotainstansi", "%" + kotainstansi + "%")
				.list();
		return tenagamedis;
	}

	@Transactional(readOnly=true)
	public List<TenagaMedis> findByProvinsi(String provinsiinstansi) {
		List<TenagaMedis> tenagamedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from TenagaMedis c where lower(c.provinsiinstansi) like lower(:provinsiinstansi)")
				.setParameter("provinsiinstansi", "%" + provinsiinstansi + "%")
				.list();
		return tenagamedis;
	}

	@Transactional(readOnly=true)
	public List<TenagaMedis> findByGolongan(String golongan) {
		List<TenagaMedis> tenagamedis = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from TenagaMedis c where lower(c.golongan) like lower(:golongan)")
				.setParameter("golongan", "%" + golongan + "%")
				.list();
		return tenagamedis;
	}

	public void save(TenagaMedis tenagaMedis) {
		this.getSessionFactory().getCurrentSession().save(tenagaMedis);
		
	}

	public void delete(TenagaMedis tenagaMedis) {
		this.getSessionFactory().getCurrentSession().delete(tenagaMedis);
		
	}

}
