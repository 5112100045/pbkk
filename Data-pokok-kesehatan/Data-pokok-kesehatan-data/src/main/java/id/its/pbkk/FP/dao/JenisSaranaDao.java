package id.its.pbkk.FP.dao;
import id.its.pbkk.FP.domain.JenisSarana;

import java.util.List;

import org.joda.time.DateTime;

public interface JenisSaranaDao {
	public List<JenisSarana> list();
	public JenisSarana findById(String id);
	public List<JenisSarana> findByJenisSarana(String Nama);
	public void save(JenisSarana jenisSarana);
	public void delete(JenisSarana jenisSarana);
}
