package id.its.pbkk.FP.dao;
import id.its.pbkk.FP.domain.RekamMedis;

import java.util.List;

import org.joda.time.DateTime;

public interface RekamMedisDao {
	public List<RekamMedis> list();
	public List<RekamMedis> findByDate(DateTime TglTransaksi);
	public List<RekamMedis> findByName(String NamaPasien);
	public List<RekamMedis> findByDiagnosa(String Diagnosa);
	public List<RekamMedis> findByPemeriksa(String Pemeriksa);
	public void save(RekamMedis rekamMedis);
	public void delete(RekamMedis rekamMedis);
}

