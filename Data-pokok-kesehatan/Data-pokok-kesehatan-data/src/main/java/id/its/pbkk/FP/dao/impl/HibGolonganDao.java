package id.its.pbkk.FP.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import id.its.pbkk.FP.dao.GolonganDao;
import id.its.pbkk.FP.domain.Golongan;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("golonganDao")
public class HibGolonganDao implements GolonganDao {
	private SessionFactory sessionFactory;
	
	public HibGolonganDao()
	{
		
	}

	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	@Transactional(readOnly=true)
	public List<Golongan> list() {
		List<Golongan> golongan = this.getSessionFactory()
				.getCurrentSession()
				.createQuery("from Golongan")
				.list();
		return golongan;
	}

	public Golongan findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Golongan> findByName(String Nama) {
		// TODO Auto-generated method stub
		return null;
	}

	public void save(Golongan golongan) {
		this.getSessionFactory().getCurrentSession().save(golongan);
		
	}

	public void delete(Golongan golongan) {
		this.getSessionFactory().getCurrentSession().delete(golongan);
		
	}
}
