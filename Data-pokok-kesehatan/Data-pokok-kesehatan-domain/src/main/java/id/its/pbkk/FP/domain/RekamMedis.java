package id.its.pbkk.FP.domain;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="rekammedis")

public class RekamMedis {

	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="idrekammedis")
	private String idRekamMedis;
	
	@ManyToOne
	@JoinColumn(name = "idtanggungan")
	private Tanggungan tanggungan;
	
	@ManyToOne
	@JoinColumn(name = "idsarana")
	private SaranaKesehatan saranaKesehatan;
	
	@ManyToOne
	@JoinColumn(name = "idpeserta")
	private PesertaJaminanKesehatan pesertaJaminanKesehatan;
	
	@Column(name="tgltransaksi", nullable=true)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime tglTransaksi;
	
	@Column(name="tempat")
	private String tempat;
	
	@Column(name="namapasien")
	private String namaPasien;
	
	@Column(name="diagnosa")
	private String diagnosa;

	@Column(name="pemeriksa")
	private String pemeriksa;
	
	@Column(name="keluhan")
	private String keluhan;
	
	@Column(name="resep")
	private String resep;
	
	@Column(name="keadaanpasienmasuk")
	private String keadaanPasienMasuk;
	
	@Column(name="keadaanpasienkeluar")
	private String keadaanPasienKeluar;
	
	@Column(name="tindakan")
	private String tindakan;
	
	@Column(name="inserted", nullable=true)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime inserted;
	
	@Column(name="updated")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime Updated;
	
	@Column(name="deleted")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime Deleted;

	public String getIdRekamMedis() {
		return idRekamMedis;
	}

	public void setIdRekamMedis(String idRekamMedis) {
		this.idRekamMedis = idRekamMedis;
	}

	public Tanggungan getTanggungan() {
		return tanggungan;
	}

	public void setTanggungan(Tanggungan tanggungan) {
		this.tanggungan = tanggungan;
	}

	public SaranaKesehatan getSaranaKesehatan() {
		return saranaKesehatan;
	}

	public void setSaranaKesehatan(SaranaKesehatan saranaKesehatan) {
		this.saranaKesehatan = saranaKesehatan;
	}

	public DateTime getTglTransaksi() {
		return tglTransaksi;
	}

	public void setTglTransaksi(DateTime tglTransaksi) {
		this.tglTransaksi = tglTransaksi;
	}

	public String getTempat() {
		return tempat;
	}

	public void setTempat(String tempat) {
		this.tempat = tempat;
	}

	public String getNamaPasien() {
		return namaPasien;
	}

	public void setNamaPasien(String namaPasien) {
		this.namaPasien = namaPasien;
	}

	public String getDiagnosa() {
		return diagnosa;
	}

	public void setDiagnosa(String diagnosa) {
		this.diagnosa = diagnosa;
	}

	public String getPemeriksa() {
		return pemeriksa;
	}

	public void setPemeriksa(String pemeriksa) {
		this.pemeriksa = pemeriksa;
	}

	public String getKeluhan() {
		return keluhan;
	}

	public void setKeluhan(String keluhan) {
		this.keluhan = keluhan;
	}

	public String getResep() {
		return resep;
	}

	public void setResep(String resep) {
		this.resep = resep;
	}

	public String getKeadaanPasienMasuk() {
		return keadaanPasienMasuk;
	}

	public void setKeadaanPasienMasuk(String keadaanPasienMasuk) {
		this.keadaanPasienMasuk = keadaanPasienMasuk;
	}

	public String getKeadaanPasienKeluar() {
		return keadaanPasienKeluar;
	}

	public void setKeadaanPasienKeluar(String keadaanPasienKeluar) {
		this.keadaanPasienKeluar = keadaanPasienKeluar;
	}

	public String getTindakan() {
		return tindakan;
	}

	public void setTindakan(String tindakan) {
		this.tindakan = tindakan;
	}

	public DateTime getInserted() {
		return inserted;
	}

	public void setInserted(DateTime inserted) {
		this.inserted = inserted;
	}

	public DateTime getUpdated() {
		return Updated;
	}

	public void setUpdated(DateTime updated) {
		Updated = updated;
	}

	public DateTime getDeleted() {
		return Deleted;
	}

	public void setDeleted(DateTime deleted) {
		Deleted = deleted;
	}
	
	
}
