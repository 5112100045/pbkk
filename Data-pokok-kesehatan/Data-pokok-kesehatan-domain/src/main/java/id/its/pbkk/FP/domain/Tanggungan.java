package id.its.pbkk.FP.domain;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="tanggungan")


public class Tanggungan {

	
	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="idtanggungan")
	private String idTanggungan;
	
	@ManyToOne
	@JoinColumn(name = "idpeserta")
	private PesertaJaminanKesehatan pesertaJaminanKesehatan;
	
	@Column(name="kepalakeluarga")
	private boolean kepalaKeluarga;
	
	@Column(name="hubungankeluarga")
	private String hubunganKeluarga;
	
	@Column(name="jeniskelamin")
	private String jenisKelamin;
	
	@Column(name="inserted", nullable=true)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime inserted;
	
	@Column(name="updated")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime updated;
	
	@Column(name="deleted")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime deleted;

	public String getIdTanggungan() {
		return idTanggungan;
	}

	public void setIdTanggungan(String idTanggungan) {
		this.idTanggungan = idTanggungan;
	}

	public PesertaJaminanKesehatan getPesertaJaminanKesehatan() {
		return pesertaJaminanKesehatan;
	}

	public void setPesertaJaminanKesehatan(PesertaJaminanKesehatan PesertaJaminanKesehatan) {
		pesertaJaminanKesehatan = pesertaJaminanKesehatan;
	}

	public boolean isKepalaKeluarga() {
		return kepalaKeluarga;
	}

	public void setKepalaKeluarga(boolean kepalaKeluarga) {
		this.kepalaKeluarga = kepalaKeluarga;
	}

	public String getHubunganKeluarga() {
		return hubunganKeluarga;
	}

	public void setHubunganKeluarga(String hubunganKeluarga) {
		this.hubunganKeluarga = hubunganKeluarga;
	}

	public String getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public DateTime getInserted() {
		return inserted;
	}

	public void setInserted(DateTime inserted) {
		this.inserted = inserted;
	}

	public DateTime getUpdated() {
		return updated;
	}

	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}

	public DateTime getDeleted() {
		return deleted;
	}

	public void setDeleted(DateTime deleted) {
		this.deleted = deleted;
	}

	/*
	@Override
	public String toString() {
		return "Tanggungan [idTanggungan=" + idTanggungan + ", Idpeserta="
				+ Idpeserta + ", kepalaKeluarga=" + kepalaKeluarga
				+ ", hubunganKeluarga=" + hubunganKeluarga + ", jenisKelamin="
				+ jenisKelamin + ", inserted=" + inserted + ", updated="
				+ updated + ", deleted=" + deleted + ", getIdTanggungan()="
				+ getIdTanggungan() + ", getIdpeserta()=" + getIdpeserta()
				+ ", isKepalaKeluarga()=" + isKepalaKeluarga()
				+ ", getHubunganKeluarga()=" + getHubunganKeluarga()
				+ ", getJenisKelamin()=" + getJenisKelamin()
				+ ", getInserted()=" + getInserted() + ", getUpdated()="
				+ getUpdated() + ", getDeleted()=" + getDeleted()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	*/
	
	
}
