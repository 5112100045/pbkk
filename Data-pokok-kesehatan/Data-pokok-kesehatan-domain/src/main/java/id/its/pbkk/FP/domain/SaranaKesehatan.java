package id.its.pbkk.FP.domain;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="saranakesehatan")
public class SaranaKesehatan 
{	
	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="idsarana")
	private String idSarana;
	
	@ManyToOne  
	@JoinColumn(name = "idjenissarana")  
	private SaranaKesehatan saranaKesehatan;
	
	@Column(name="namasarana", length=30, nullable=false)
	private String namaSarana;
	
	@Column(name="alamatsarana", length=100)
	private String alamatSarana;
	
	@Column(name="kotasarana", length=50)
	private String kotaSarana;
	
	@Column(name="provinsisarana", length=50)
	private String provinsiSarana;

	@Column(name="inserted", nullable=true)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime inserted;
	
	@Column(name="updated")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime ipdated;
	
	@Column(name="deleted")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime deleted;

	public String getIdSarana() {
		return idSarana;
	}

	public void setIdSarana(String idSarana) {
		this.idSarana = idSarana;
	}

	public SaranaKesehatan getSaranaKesehatan() {
		return saranaKesehatan;
	}

	public void setSaranaKesehatan(SaranaKesehatan saranaKesehatan) {
		this.saranaKesehatan = saranaKesehatan;
	}

	public String getNamaSarana() {
		return namaSarana;
	}

	public void setNamaSarana(String namaSarana) {
		this.namaSarana = namaSarana;
	}

	public String getAlamatSarana() {
		return alamatSarana;
	}

	public void setAlamatSarana(String alamatSarana) {
		this.alamatSarana = alamatSarana;
	}

	public String getKotaSarana() {
		return kotaSarana;
	}

	public void setKotaSarana(String kotaSarana) {
		this.kotaSarana = kotaSarana;
	}

	public String getProvinsiSarana() {
		return provinsiSarana;
	}

	public void setProvinsiSarana(String provinsiSarana) {
		this.provinsiSarana = provinsiSarana;
	}

	public DateTime getInserted() {
		return inserted;
	}

	public void setInserted(DateTime inserted) {
		this.inserted = inserted;
	}

	public DateTime getIpdated() {
		return ipdated;
	}

	public void setIpdated(DateTime ipdated) {
		this.ipdated = ipdated;
	}

	public DateTime getDeleted() {
		return deleted;
	}

	public void setDeleted(DateTime deleted) {
		this.deleted = deleted;
	}

	/*
	@Override
	public String toString() {
		return "SaranaKesehatan [idSarana=" + idSarana + ", idJenisSarana="
				+ idJenisSarana + ", namaSarana=" + namaSarana
				+ ", alamatSarana=" + alamatSarana + ", kotaSarana="
				+ kotaSarana + ", provinsiSarana=" + provinsiSarana
				+ ", inserted=" + inserted + ", ipdated=" + ipdated
				+ ", deleted=" + deleted + ", getIdSarana()=" + getIdSarana()
				+ ", getIdJenisSarana()=" + getIdJenisSarana()
				+ ", getNamaSarana()=" + getNamaSarana()
				+ ", getAlamatSarana()=" + getAlamatSarana()
				+ ", getKotaSarana()=" + getKotaSarana()
				+ ", getProvinsiSarana()=" + getProvinsiSarana()
				+ ", getInserted()=" + getInserted() + ", getIpdated()="
				+ getIpdated() + ", getDeleted()=" + getDeleted()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	*/
	
	
}
