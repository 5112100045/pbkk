package id.its.pbkk.FP.domain;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
//import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="jenissarana")
public class JenisSarana
{
	public JenisSarana() {
		
	}
	public JenisSarana(String namasarana) {
		
		this.setNamaJenisSarana(namasarana);
	}

	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="idjenissarana")
	private String idJenisSarana;
	
	@Column(name="namajenissarana", length=30, nullable=false)
	private String namaJenisSarana;

	public String getIdJenisSarana() {
		return idJenisSarana;
	}
	public void setIdJenisSarana(String idJenisSarana) {
		this.idJenisSarana = idJenisSarana;
	}
	public String getNamaJenisSarana() {
		return namaJenisSarana;
	}
	public void setNamaJenisSarana(String namaJenisSarana) {
		this.namaJenisSarana = namaJenisSarana;
	}


	

}
