package id.its.pbkk.FP.domain;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="datarujukan")

public class DataRujukan {

	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="idrujukan")
	private String IdRujukan;
	
	@ManyToOne
	@JoinColumn(name = "idsarana")
	private SaranaKesehatan saranaKesehatan;
	
	@ManyToOne
	@JoinColumn(name = "idtenagamedis")
	private TenagaMedis tenagaMedis;
	
	@Column(name="inserted", nullable=true)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime inserted;
	
	@Column(name="updated")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime updated;
	
	@Column(name="deleted")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime deleted;
	
	@Column(name="isirujukan")
	private String isiRujukan;
	
	@Column(name="tanggalrujukan")
	private DateTime tanggalRujukan;
	
	@Column(name="keadaanpasien")
	private String keadaanPasien ;

	public String getIdRujukan() {
		return IdRujukan;
	}

	public void setIdRujukan(String idRujukan) {
		IdRujukan = idRujukan;
	}

	public SaranaKesehatan getSaranaKesehatan() {
		return saranaKesehatan;
	}

	public void setSaranaKesehatan(SaranaKesehatan saranaKesehatan) {
		this.saranaKesehatan = saranaKesehatan;
	}

	public TenagaMedis getTenagaMedis() {
		return tenagaMedis;
	}

	public void setTenagaMedis(TenagaMedis tenagaMedis) {
		this.tenagaMedis = tenagaMedis;
	}

	public DateTime getInserted() {
		return inserted;
	}

	public void setInserted(DateTime inserted) {
		this.inserted = inserted;
	}

	public DateTime getUpdated() {
		return updated;
	}

	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}

	public DateTime getDeleted() {
		return deleted;
	}

	public void setDeleted(DateTime deleted) {
		this.deleted = deleted;
	}

	public String getIsiRujukan() {
		return isiRujukan;
	}

	public void setIsiRujukan(String isiRujukan) {
		this.isiRujukan = isiRujukan;
	}

	public DateTime getTanggalRujukan() {
		return tanggalRujukan;
	}

	public void setTanggalRujukan(DateTime tanggalRujukan) {
		this.tanggalRujukan = tanggalRujukan;
	}

	public String getKeadaanPasien() {
		return keadaanPasien;
	}

	public void setKeadaanPasien(String keadaanPasien) {
		this.keadaanPasien = keadaanPasien;
	}

	/*
	@Override
	public String toString() {
		return "DataRujukan [IdRujukan=" + IdRujukan + ", inserted=" + inserted
				+ ", updated=" + updated + ", deleted=" + deleted
				+ ", isiRujukan=" + isiRujukan + ", tanggalRujukan="
				+ tanggalRujukan + ", keadaanPasien=" + keadaanPasien
				+ ", getIdRujukan()=" + getIdRujukan() + ", getInserted()="
				+ getInserted() + ", getUpdated()=" + getUpdated()
				+ ", getDeleted()=" + getDeleted() + ", getIsiRujukan()="
				+ getIsiRujukan() + ", getTanggalRujukan()="
				+ getTanggalRujukan() + ", getKeadaanPasien()="
				+ getKeadaanPasien() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	*/
	
	
}
