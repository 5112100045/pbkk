package id.its.pbkk.FP.domain;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.joda.time.DateTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
@Table(name="golongan")
public class Golongan {
	
	public Golongan()
	{
		
	}
	
	public Golongan(String namagolongan)
	{
		this.setNamagolongan(namagolongan);
		this.setInserted(DateTime.now());
	}
	
	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="idgolongan")
	private UUID idgolongan;
	
	@Column(name="namagolongan", length=20, nullable=false)
	private String namagolongan;
	
	@Column(name="expdate")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime expdate;
	
	@Column(name="inserted")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime inserted;
	
	@Column(name="updated")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime updated;
	
	@Column(name="deleted")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime deleted;

	public UUID getIdgolongan() {
		return idgolongan;
	}

	public void setIdgolongan(UUID idgolongan) {
		this.idgolongan = idgolongan;
	}

	public String getNamagolongan() {
		return namagolongan;
	}

	public void setNamagolongan(String namagolongan) {
		this.namagolongan = namagolongan;
	}

	public DateTime getExpdate() {
		return expdate;
	}

	public void setExpdate(DateTime expdate) {
		this.expdate = expdate;
	}

	public DateTime getInserted() {
		return inserted;
	}

	public void setInserted(DateTime inserted) {
		this.inserted = inserted;
	}

	public DateTime getUpdated() {
		return updated;
	}

	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}

	public DateTime getDeleted() {
		return deleted;
	}

	public void setDeleted(DateTime deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Golongan [idgolongan=");
		builder.append(idgolongan);
		builder.append(", namagolongan=");
		builder.append(namagolongan);
		builder.append(", expdate=");
		builder.append(expdate);
		builder.append(", inserted=");
		builder.append(inserted);
		builder.append(", updated=");
		builder.append(updated);
		builder.append(", deleted=");
		builder.append(deleted);
		builder.append("]");
		return builder.toString();
	}

	
	
	
}
