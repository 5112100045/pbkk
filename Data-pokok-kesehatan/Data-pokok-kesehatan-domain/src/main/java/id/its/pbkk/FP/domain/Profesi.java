package id.its.pbkk.FP.domain;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.joda.time.DateTime;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
@Table(name="profesi")
public class Profesi {
	
	public Profesi()
	{
		
	}
	
	public Profesi(String namaprofesi)
	{
		this.setNamaprofesi(namaprofesi);
		this.setInserted(DateTime.now());
	}
	
	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="idprofesi")
	private UUID idprofesi;
	
	@Column(name="namaprofesi", length=20, nullable=false)
	private String namaprofesi;
	
	@Column(name="expdate")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime expdate;
	
	@Column(name="inserted")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime inserted;
	
	@Column(name="updated")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime updated;
	
	@Column(name="deleted")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime deleted;

	public UUID getIdprofesi() {
		return idprofesi;
	}

	public void setIdprofesi(UUID idprofesi) {
		this.idprofesi = idprofesi;
	}

	public String getNamaprofesi() {
		return namaprofesi;
	}

	public void setNamaprofesi(String namaprofesi) {
		this.namaprofesi = namaprofesi;
	}

	public DateTime getExpdate() {
		return expdate;
	}

	public void setExpdate(DateTime expdate) {
		this.expdate = expdate;
	}

	public DateTime getInserted() {
		return inserted;
	}

	public void setInserted(DateTime inserted) {
		this.inserted = inserted;
	}

	public DateTime getUpdated() {
		return updated;
	}

	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}

	public DateTime getDeleted() {
		return deleted;
	}

	public void setDeleted(DateTime deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Profesi [idprofesi=");
		builder.append(idprofesi);
		builder.append(", namaprofesi=");
		builder.append(namaprofesi);
		builder.append(", expdate=");
		builder.append(expdate);
		builder.append(", inserted=");
		builder.append(inserted);
		builder.append(", updated=");
		builder.append(updated);
		builder.append(", deleted=");
		builder.append(deleted);
		builder.append("]");
		return builder.toString();
	}

	
}
