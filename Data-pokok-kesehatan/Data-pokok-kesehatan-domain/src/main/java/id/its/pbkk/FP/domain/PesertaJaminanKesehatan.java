package id.its.pbkk.FP.domain;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;


@Entity
@Table(name="pesertajaminankesehatan")
public class PesertaJaminanKesehatan 
{
	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name="idpeserta")
	private String idpeserta;
	
	@Column(name="namapeserta")
	private String namapeserta;
	
	@Column(name="jeniskepesertaan")
	private String jeniskepesertaan;
	
	@Column(name="jeniskelamin")
	private String jeniskelamin;
	
	@Column(name="inserted", nullable=true)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime inserted;
	
	@Column(name="Updated")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime Updated;
	
	@Column(name="Deleted")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime Deleted;
	
	public PesertaJaminanKesehatan()
	{
		
	}
	
	

	public PesertaJaminanKesehatan(String namapeserta, String jeniskepesertaan, String jeniskelamin)
	{
		this.setNamapeserta(namapeserta);
		this.setJeniskepesertaan(jeniskepesertaan);
		this.setJeniskelamin(jeniskelamin);
		this.setInserted(DateTime.now());
	}

	public String getIdpeserta() {
		return idpeserta;
	}

	public void setIdpeserta(String idpeserta) {
		this.idpeserta = idpeserta;
	}

	public String getNamapeserta() {
		return namapeserta;
	}

	public void setNamapeserta(String namapeserta) {
		this.namapeserta = namapeserta;
	}

	public String getJeniskepesertaan() {
		return jeniskepesertaan;
	}

	public void setJeniskepesertaan(String jeniskepesertaan) {
		this.jeniskepesertaan = jeniskepesertaan;
	}

	public String getJeniskelamin() {
		return jeniskelamin;
	}

	public void setJeniskelamin(String jeniskelamin) {
		this.jeniskelamin = jeniskelamin;
	}

	public DateTime getInserted() {
		return inserted;
	}

	public void setInserted(DateTime inserted) {
		this.inserted = inserted;
	}

	public DateTime getUpdated() {
		return Updated;
	}

	public void setUpdated(DateTime updated) {
		Updated = updated;
	}

	public DateTime getDeleted() {
		return Deleted;
	}

	public void setDeleted(DateTime deleted) {
		Deleted = deleted;
	}
	
	@Override
	public String toString() {
		return "PesertaJaminanKesehatan [idpeserta=" + idpeserta
				+ ", namapeserta=" + namapeserta + ", jeniskepesertaan="
				+ jeniskepesertaan + ", jeniskelamin=" + jeniskelamin
				+ ", inserted=" + inserted + ", Updated=" + Updated
				+ ", Deleted=" + Deleted + ", getIdpeserta()=" + getIdpeserta()
				+ ", getNamapeserta()=" + getNamapeserta()
				+ ", getJeniskepesertaan()=" + getJeniskepesertaan()
				+ ", getJeniskelamin()=" + getJeniskelamin()
				+ ", getInserted()=" + getInserted() + ", getUpdated()="
				+ getUpdated() + ", getDeleted()=" + getDeleted()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
	
}
