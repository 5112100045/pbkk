package id.its.pbkk.FP.domain;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name="tenagamedis")
public class TenagaMedis {

	@Id
	@GeneratedValue(generator="uuid2")
	@GenericGenerator(name="uuid2", strategy="uuid2")
	@Column(name = "idtenagamedis")
	private String idTenagaMedis;
	
	@ManyToOne
	@JoinColumn(name = "idsarana")
	private SaranaKesehatan saranaKesehatan;
	
	@Column(name = "namatenagamedis")
	private String namaTenagaMedis;
	
	@Column(name = "alamattenagamedis")
	private String alamatTenagaMedis;
	
	@ManyToOne  
	@JoinColumn(name = "idprofesi")  
	private Profesi profesi;
	
	@ManyToOne  
	@JoinColumn(name = "idgolongan")  
	private Golongan golongan;
	
	@Column(name="alamatinstansi")
	private String alamatInstansi;
	
	@Column(name="kotainstansi")
	private String kotaInstansi;
	
	@Column(name="provinsiinstansi")
	private String provinsiInstansi;
	
	@Column(name="inserted", nullable=true)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime inserted;
	
	@Column(name="updated")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime updated;
	
	@Column(name="deleted")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime deleted;

	public String getIdTenagaMedis() {
		return idTenagaMedis;
	}

	public void setIdTenagaMedis(String idTenagaMedis) {
		this.idTenagaMedis = idTenagaMedis;
	}

	public SaranaKesehatan getSaranaKesehatan() {
		return saranaKesehatan;
	}

	public void setSaranaKesehatan(SaranaKesehatan saranaKesehatan) {
		this.saranaKesehatan = saranaKesehatan;
	}

	public String getNamaTenagaMedis() {
		return namaTenagaMedis;
	}

	public void setNamaTenagaMedis(String namaTenagaMedis) {
		this.namaTenagaMedis = namaTenagaMedis;
	}

	public String getAlamatTenagaMedis() {
		return alamatTenagaMedis;
	}

	public void setAlamatTenagaMedis(String alamatTenagaMedis) {
		this.alamatTenagaMedis = alamatTenagaMedis;
	}

	public Profesi getProfesi() {
		return profesi;
	}

	public void setProfesi(Profesi profesi) {
		this.profesi = profesi;
	}

	public Golongan getGolongan() {
		return golongan;
	}

	public void setGolongan(Golongan golongan) {
		this.golongan = golongan;
	}

	public String getAlamatInstansi() {
		return alamatInstansi;
	}

	public void setAlamatInstansi(String alamatInstansi) {
		this.alamatInstansi = alamatInstansi;
	}

	public String getKotaInstansi() {
		return kotaInstansi;
	}

	public void setKotaInstansi(String kotaInstansi) {
		this.kotaInstansi = kotaInstansi;
	}

	public String getProvinsiInstansi() {
		return provinsiInstansi;
	}

	public void setProvinsiInstansi(String provinsiInstansi) {
		this.provinsiInstansi = provinsiInstansi;
	}

	public DateTime getInserted() {
		return inserted;
	}

	public void setInserted(DateTime inserted) {
		this.inserted = inserted;
	}

	public DateTime getUpdated() {
		return updated;
	}

	public void setUpdated(DateTime updated) {
		this.updated = updated;
	}

	public DateTime getDeleted() {
		return deleted;
	}

	public void setDeleted(DateTime deleted) {
		this.deleted = deleted;
	}

	/*
	@Override
	public String toString() {
		return "TenagaMedis [idTenagaMedis=" + idTenagaMedis + ", idSarana="
				+ idSarana + ", namaTenagaMedis=" + namaTenagaMedis
				+ ", alamatTenagaMedis=" + alamatTenagaMedis + ", profesi="
				+ profesi + ", golongan=" + golongan + ", alamatInstansi="
				+ alamatInstansi + ", kotaInstansi=" + kotaInstansi
				+ ", provinsiInstansi=" + provinsiInstansi + ", inserted="
				+ inserted + ", updated=" + updated + ", deleted=" + deleted
				+ ", getIdTenagaMedis()=" + getIdTenagaMedis()
				+ ", getIdSarana()=" + getIdSarana()
				+ ", getNamaTenagaMedis()=" + getNamaTenagaMedis()
				+ ", getAlamatTenagaMedis()=" + getAlamatTenagaMedis()
				+ ", getProfesi()=" + getProfesi() + ", getGolongan()="
				+ getGolongan() + ", getAlamatInstansi()="
				+ getAlamatInstansi() + ", getKotaInstansi()="
				+ getKotaInstansi() + ", getProvinsiInstansi()="
				+ getProvinsiInstansi() + ", getInserted()=" + getInserted()
				+ ", getUpdated()=" + getUpdated() + ", getDeleted()="
				+ getDeleted() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}
	*/
	
	
}

