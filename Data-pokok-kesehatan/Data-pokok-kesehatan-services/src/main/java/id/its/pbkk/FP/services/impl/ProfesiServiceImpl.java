package id.its.pbkk.FP.services.impl;

import java.util.List;




import id.its.pbkk.FP.dao.ProfesiDao;
import id.its.pbkk.FP.domain.Profesi;
import id.its.pbkk.FP.services.ProfesiService;

public class ProfesiServiceImpl implements ProfesiService {
	private ProfesiDao profesiDao;
	
	public ProfesiServiceImpl()
	{
		
	}
	
	public List<Profesi> list()
	{
		return this.profesiDao.list();
	}
	
	public void createProfesi (Profesi newProfesi)
	{
		//this.profesiDao.save.(newProfesi);
	}
	
	public ProfesiDao getProfesiDao()
	{
		return profesiDao;
	}
	
	public void setProfesiDao (ProfesiDao profesiDao)
	{
		this.profesiDao=profesiDao;
	}
}
