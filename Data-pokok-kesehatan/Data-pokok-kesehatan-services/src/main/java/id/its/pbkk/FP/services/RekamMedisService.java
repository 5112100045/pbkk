package id.its.pbkk.FP.services;

import id.its.pbkk.FP.domain.RekamMedis;
import java.util.List;

public interface RekamMedisService {
	List<RekamMedis> list();
	void createRekamMedis(RekamMedis newRekamMedis);
}
