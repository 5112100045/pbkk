package id.its.pbkk.FP.services.impl;

import java.util.List;

import id.its.pbkk.FP.dao.TanggunganDao;
import id.its.pbkk.FP.domain.Tanggungan;
import id.its.pbkk.FP.services.TanggunganService;

public class TanggunganServiceImpl implements TanggunganService {
	private TanggunganDao tanggunganDao;
	
	public TanggunganServiceImpl()
	{
		
	}
	
	public List<Tanggungan> list()
	{
		return this.tanggunganDao.list();
	}
	public void createTanggungan(Tanggungan newTanggungan)
	{
		this.tanggunganDao.save(newTanggungan);
	}
	
	public TanggunganDao getTanggunganDao()
	{
		return this.tanggunganDao;
	}
	public void setTanggunganDao (TanggunganDao tanggunganDao)
	{
		this.tanggunganDao=tanggunganDao;
	}
	

}
