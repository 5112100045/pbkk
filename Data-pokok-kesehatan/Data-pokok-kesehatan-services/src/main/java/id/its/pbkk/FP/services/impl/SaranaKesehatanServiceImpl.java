package id.its.pbkk.FP.services.impl;

import java.util.List;

import id.its.pbkk.FP.dao.SaranaKesehatanDao;
import id.its.pbkk.FP.domain.SaranaKesehatan;
import id.its.pbkk.FP.services.SaranaKesehatanService;

public class SaranaKesehatanServiceImpl implements SaranaKesehatanService {
	private SaranaKesehatanDao saranaKesehatanDao;
	
	public SaranaKesehatanServiceImpl()
	{
		
	}
	
	public List<SaranaKesehatan> list()
	{
		return this.saranaKesehatanDao.list();
	}
	
	public void createSaranaKesehatan (SaranaKesehatan newSaranaKesehatan)
	{
		this.saranaKesehatanDao.save.(newSaranaKesehatan);
	}
	
	public SaranaKesehatanDao getSaranaKesehatanDao()
	{
		return saranaKesehatanDao;
	}
	
	public void setSaranaKesehatanDao (SaranaKesehatanDao saranaKesehatanDao)
	{
		this.saranaKesehatanDao=saranaKesehatanDao;
	}
	
}
