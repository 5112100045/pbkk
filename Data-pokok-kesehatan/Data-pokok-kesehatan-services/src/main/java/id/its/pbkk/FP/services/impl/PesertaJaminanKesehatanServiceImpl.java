package id.its.pbkk.FP.services.impl;

import java.util.List;

import id.its.pbkk.FP.dao.PesertaJaminanKesehatanDao;
import id.its.pbkk.FP.domain.PesertaJaminanKesehatan;
import id.its.pbkk.FP.services.PesertaJaminanKesehatanService;

public class PesertaJaminanKesehatanServiceImpl implements PesertaJaminanKesehatanService {
	private PesertaJaminanKesehatanDao pesertaJaminanKesehatanDao;

	public PesertaJaminanKesehatanServiceImpl()
	{
		
	}
	
	public List<PesertaJaminanKesehatan> list()
	{
		return this.PesertaJaminanKesehatanDao.list();
	}
	
	public void createPesertaJaminanKesehatan(PesertaJaminanKesehatan newPesertaJaminanKesehatan)
	{
		this.pesertaJaminanKesehatanDao.save(newPesertaJaminanKesehatan);
	}
	
	public PesertaJaminanKesehatanDao getPesertaJaminanKesehatanDao()
	{
		return pesertaJaminanKesehatanDao;
	}
	
	public void setPesertaJaminanKesehatanDao (PesertaJaminanKesehatanDao PesertaJaminanKesehatanDao)
	{
		this.pesertaJaminanKesehatanDao=pesertaJaminanKesehatanDao;
	}

}
