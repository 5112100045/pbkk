package id.its.pbkk.FP.services;

import id.its.pbkk.FP.domain.JenisSarana;
import java.util.List;

public interface JenisSaranaService {
	List<JenisSarana> list();
	void createJenisSarana(JenisSarana newJenisSarana);
}
