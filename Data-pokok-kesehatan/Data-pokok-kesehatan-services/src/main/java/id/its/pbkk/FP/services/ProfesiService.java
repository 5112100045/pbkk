package id.its.pbkk.FP.services;

import id.its.pbkk.FP.domain.Profesi;
import java.util.List;

public interface ProfesiService {
	List<Profesi> list();
	void createProfesi(Profesi newProfesi);
}
