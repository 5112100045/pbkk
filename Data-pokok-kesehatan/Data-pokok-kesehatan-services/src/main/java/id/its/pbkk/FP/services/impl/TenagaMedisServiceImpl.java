package id.its.pbkk.FP.services.impl;

import java.util.List;

import id.its.pbkk.FP.dao.TanggunganDao;
import id.its.pbkk.FP.dao.TenagaMedisDao;
import id.its.pbkk.FP.domain.TenagaMedis;
import id.its.pbkk.FP.services.TenagaMedisService;

public class TenagaMedisServiceImpl implements TenagaMedisService {
	private TenagaMedisDao tenagaMedisDao;
	
	public TenagaMedisServiceImpl()
	{
		
	}
	
	public List<TenagaMedis> list()
	{
		return this.tenagaMedisDao.list();
	}
	public void createTenagaMedis(TenagaMedis newTenagaMedis)
	{
		this.tenagaMedisDao.save(newTenagaMedis);
	}
	public TenagaMedisDao getTenagaMedisDao()
	{
		return tenagaMedisDao;
	}
	public void setTenagaMedisDao (TenagaMedisDao tenagaMedisDao)
	{
		this.tenagaMedisDao=tenagaMedisDao;
	}

}
