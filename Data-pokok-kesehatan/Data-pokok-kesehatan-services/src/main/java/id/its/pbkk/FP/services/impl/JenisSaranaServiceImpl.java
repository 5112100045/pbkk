package id.its.pbkk.FP.services.impl;

import java.util.List;

import id.its.pbkk.FP.dao.JenisSaranaDao;
import id.its.pbkk.FP.domain.JenisSarana;
import id.its.pbkk.FP.services.JenisSaranaService;

public class JenisSaranaServiceImpl implements JenisSaranaService {
	private JenisSaranaDao jenisSaranaDao;
	
	public JenisSaranaServiceImpl()
	{
		
	}
	
	public List<JenisSarana> list()
	{
		return this.jenisSaranaDao.list();
	}
	public void createJenisSarana(JenisSarana newJenisSarana)
	{
		this.jenisSaranaDao.save(newJenisSarana);
	}
	public JenisSaranaDao getJenisSaranaDao()
	{
		return jenisSaranaDao;
	}
	public void setJenisSaranaDao (JenisSaranaDao jenisSaranaDao)
	{
		this.jenisSaranaDao=jenisSaranaDao;
	}
	
}
