package id.its.pbkk.FP.services;

import id.its.pbkk.FP.domain.PesertaJaminanKesehatan;
import java.util.List;

public interface PesertaJaminanKesehatanService {
	List<PesertaJaminanKesehatan> list();
	void createPesertaJaminanKesehatan(PesertaJaminanKesehatan newPesertaJaminanKesehatan);
}
