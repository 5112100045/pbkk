package id.its.pbkk.FP.services;

import id.its.pbkk.FP.domain.TenagaMedis;
import java.util.List;

public interface TenagaMedisService {
	List<TenagaMedis> list();
	void createTenagaMedis(TenagaMedis newTenagaMedis);
}
