package id.its.pbkk.FP.services;

import id.its.pbkk.FP.domain.Golongan;
import java.util.List;

public interface GolonganService {
	List<Golongan> list();
	void createGolongan(Golongan newGolongan);
}
