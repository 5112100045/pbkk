package id.its.pbkk.FP.services.impl;

import java.util.List;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import id.its.pbkk.FP.dao.DataRujukanDao;
import id.its.pbkk.FP.domain.DataRujukan;
import id.its.pbkk.FP.services.DataRujukanService;

public class DataRujukanServiceImpl implements DataRujukanService
{
	private DataRujukanDao dataRujukanDao;
	
	public DataRujukanServiceImpl()
	{
		
	}

	public List<DataRujukan> list()
	{
		return this.dataRujukanDao.list();
	}
	
	public void createDataRujukan (DataRujukan newDataRujukan)
	{
		this.dataRujukanDao.save(newDataRujukan);
	}
	
	public DataRujukanDao getDataRujukanDao()
	{
		return dataRujukanDao;
	}
	
	public void setDataRujukanDao (DataRujukanDao dataRujukanDao)
	{
		this.dataRujukanDao = dataRujukanDao;
	}
	
}
