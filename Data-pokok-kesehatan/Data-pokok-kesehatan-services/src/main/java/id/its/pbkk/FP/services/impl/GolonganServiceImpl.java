package id.its.pbkk.FP.services.impl;

import java.util.List;

import id.its.pbkk.FP.dao.GolonganDao;
import id.its.pbkk.FP.domain.Golongan;
import id.its.pbkk.FP.services.GolonganService;

public class GolonganServiceImpl implements GolonganService{
	private GolonganDao golonganDao;
	
	public GolonganServiceImpl()
	{
		
	}
	
	public List<Golongan> list()
	{
		return this.golonganDao.list();
	}
	
	public void createGolongan (Golongan newGolongan)
	{
		this.golonganDao.save.(newGolongan);
	}
	
	public GolonganDao getGolonganDao()
	{
		return golonganDao;
	}
	
	public void setGolonganDao (GolonganDao golonganDao)
	{
		this.golonganDao=golonganDao;
	}	
}
