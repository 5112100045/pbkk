package id.its.pbkk.FP.services;

import id.its.pbkk.FP.domain.SaranaKesehatan;
import java.util.List;

public interface SaranaKesehatanService {
	List<SaranaKesehatan> list();
	void createSaranaKesehatan(SaranaKesehatan newSaranaKesehatan);
}
