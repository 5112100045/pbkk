package id.its.pbkk.FP.services;

import id.its.pbkk.FP.domain.Tanggungan;
import java.util.List;

public interface TanggunganService {
	List<Tanggungan> list();
	void createTanggungan(Tanggungan newTanggungan);
}
