package id.its.pbkk.FP.services;

import id.its.pbkk.FP.domain.DataRujukan;
import java.util.List;

public interface DataRujukanService {
	List<DataRujukan> list();
	void createDataRujukan(DataRujukan newDataRujukan);
}
