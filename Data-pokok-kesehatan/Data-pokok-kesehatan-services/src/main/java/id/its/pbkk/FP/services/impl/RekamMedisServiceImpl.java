package id.its.pbkk.FP.services.impl;

import java.util.List;

import id.its.pbkk.FP.dao.RekamMedisDao;
import id.its.pbkk.FP.services.RekamMedisService;

public class RekamMedisServiceImpl implements RekamMedisService {
	private RekamMedisDao rekamMedisDao;
	
	public RekamMedisServiceImpl()
	{
		
	}
	
	public List<RekamMedis> list()
	{
		return this.rekamMedisDao.list();
	}
	
	public void createRekamMedis (RekamMedis newRekamMedis)
	{
		this.rekamMedisDao.save.(newRekamMedis);
	}
	
	public RekamMedisDao getRekamMedisDao()
	{
		return rekamMedisDao;
	}
	
	public void setRekamMedisDao (RekamMedisDao rekamMedisDao)
	{
		this.rekamMedisDao=rekamMedisDao;
	}
	public List<RekamMedis> list()
	{
		return null;
	}
}
